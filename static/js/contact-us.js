var frm = $('#message-form');
    
function resetSubmitBtnText() {
    $("#form-submit-btn .spinner-border").remove();
    $("#form-submit-btn .btn__text").text('Send Message');
    $("#form-submit-btn").removeClass("btn--loading");    
}

function outputFormStatus(status, text) {
    $("#form-status").html( `
    <div class="alert alert-${status} alert-dismissible fade show" role="alert">
        ${text}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">
                <i class="lni lni-close"></i>
            </span>
        </button>
    </div>`);
}

frm.submit(function (e) {
    
    e.preventDefault();

    $("#form-submit-btn").prepend('<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>');
    $("#form-submit-btn .btn__text").text('Sending Message...');
    $("#form-submit-btn").addClass("btn--loading");

    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            
            resetSubmitBtnText();              
            outputFormStatus('success', '✔️ Thank you for reaching out!, The mail has been sent successfully.');

        },
        error: function (data) {            
            resetSubmitBtnText();  
            outputFormStatus('danger', '❌ There was a problem trying to send your message. Please try again later.');
        },
    });
});