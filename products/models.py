import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField

from .utils import get_unique_slug, get_main_image_filename, get_image_filename


class Category(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=50, blank=True)
    slug = models.SlugField(max_length=140, unique=True,
                            blank=True, allow_unicode=True, verbose_name=_('Slug'))
    created_at = models.DateTimeField(verbose_name=_("Create Date and Time"), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_("Update Date and Time"), auto_now=True)

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def save(self, *args, **kwargs):
        self.create_slug()
        super().save()

    def create_slug(self):
        if not self.slug:
            self.slug = get_unique_slug(self, 'title', 'slug')

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(verbose_name=_("Title"), max_length=100, blank=True)

    category = models.ForeignKey('Category', verbose_name=_("Category"), related_name='products', blank=True, on_delete=models.CASCADE)
    photo = models.ImageField(verbose_name=_("Photo"), upload_to=get_main_image_filename)
    slug = models.SlugField(max_length=140, unique=True,
                            blank=True, allow_unicode=True, verbose_name=_('Slug'))
    description = RichTextField(verbose_name=_("Description"), blank=True)
    created_at = models.DateTimeField(verbose_name=_("Create Date and Time"), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_("Update Date and Time"), auto_now=True)

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")
        ordering = ('-created_at', )

    def save(self, *args, **kwargs):
        self.create_slug()
        super().save()

    def create_slug(self):
        if not self.slug:
            self.slug = get_unique_slug(self, 'title', 'slug')

    def __str__(self):
        return f'{self.title}'


class Photo(models.Model):
    product = models.ForeignKey("Product", verbose_name=_("Product"), related_name='photos', on_delete=models.CASCADE)
    photo = models.ImageField(verbose_name=_("Photo"), upload_to=get_image_filename)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    class Meta:
        verbose_name = _("Photo")
        verbose_name_plural = _("Photos")

    def __str__(self):
        return self.photo.name.replace('uploads/products/', '')

