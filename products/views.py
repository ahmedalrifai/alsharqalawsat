from django.views.generic import ListView, DetailView
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import get_object_or_404

from .models import Product, Category
from .filters import ProductFilter


class ProductListView(ListView):
    model = Product
    template_name = 'product/list.html'
    context_object_name = 'products'
    paginate_by = 9

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        
        categories = Category.objects.all()
        category_id= self.request.GET.get('category')
        print(category_id)
        if category_id != '':
            products = Product.objects.filter(category__id=category_id)
        if category_id == '' or category_id is None:
            products = Product.objects.all()

        page = self.request.GET.get('page')
        paginator = Paginator(products, self.paginate_by)

        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)

        context['products'] = products
        context['categories'] = categories
        context['num_pages'] = paginator.num_pages

        return context


class ProductDetailView(DetailView):
    model = Product 
    template_name = 'product/detail.html'
    context_object_name = 'product'

    def get_object(self):
        return get_object_or_404(Product, slug=self.kwargs['product_slug'])
