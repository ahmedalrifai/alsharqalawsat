from .dev import *


SECRET_KEY = os.environ.get('SECRET_KEY')
DEBUG = False


# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),
    }
}


# Hosts
ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS').split(';')


# Email
EMAIL_HOST = os.environ.get('EMAIL_HOST')
EMAIL_PORT = os.environ.get('EMAIL_PORT')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
DEFAULT_FROM_EMAIL = os.environ.get('DEFAULT_FROM_EMAIL')

EMAIL_USE_TLS = False
if os.environ.get('EMAIL_USE_TLS') == '1':
    EMAIL_USE_TLS = True


# Maintenance Mode
MAINTENANCE_MODE = False
MAINTENANCE_MODE_IGNORE_STAFF = False
MAINTENANCE_MODE_IGNORE_SUPERUSER = False


MEDIA_ROOT = '/home/alsharqa/public_html/media'
STATIC_ROOT = '/home/alsharqa/public_html/static'
