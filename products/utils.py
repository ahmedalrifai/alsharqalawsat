import time as _time

from django.utils.text import slugify


def get_unique_slug(model_instance, slugable_field_name, slug_field_name):
    '''
    Takes a model instance, sluggable field name (such as 'title') of that
    model as string, slug field name (such as 'slug') of the model as string;
    returns a unique slug as string.
    '''
    slug = slugify(getattr(model_instance, slugable_field_name),
                   allow_unicode=True)
    unique_slug = slug
    extension = 1
    ModelClass = model_instance.__class__

    while ModelClass._default_manager.filter(
            **{slug_field_name: unique_slug}
    ).exists():
        unique_slug = f'{slug}-{extension}'
        extension += 1

    return unique_slug

def get_image_filename(instance, filename):
    title = instance.product.title
    slug = slugify(title)
    ext = filename.split('.')[-1]
    time = _time.strftime("%Y-%m-%d")
    return f"uploads/products/Product-{time}-{instance.uuid}.{ext}" 


def get_main_image_filename(instance, filename):
    title = instance.title
    slug = slugify(title)
    ext = filename.split('.')[-1]
    time = _time.strftime("%Y-%m-%d")
    model_name = instance.__class__._meta.verbose_name
    return f"uploads/products/{model_name}-Main-Pic-{slug}-{time}-.{ext}" 
