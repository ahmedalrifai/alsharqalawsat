from django.contrib import admin

from .models import Category, Product, Photo


class PhotoInline(admin.TabularInline):
    model = Photo
    readonly_fields = ['photo']
    extra = 1


class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'created_at', 'updated_at')
    fields = ('title', 'slug', 'category', 'description', 'photo', 'created_at', 'updated_at')
    readonly_fields=  ('slug', 'created_at', 'updated_at',)
    list_filter = ('category', 'created_at')
    search_fields = ('title',)
    inlines = [PhotoInline ]
    
    def save_model(self, request, obj, form, change):
        obj.save()
 
        for afile in request.FILES.getlist('photos_multiple'):
            obj.photos.create(photo=afile)


class CategorytAdmin(admin.ModelAdmin):
    fields = ('title', 'slug',)
    search_fields = ('title',)
    readonly_fields=  ('slug',)



admin.site.register(Category, CategorytAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Photo)
