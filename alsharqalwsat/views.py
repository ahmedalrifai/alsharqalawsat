from django.views.generic import ListView, DetailView
from django.shortcuts import get_object_or_404, render, redirect
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives, send_mail

from maintenance_mode.decorators import force_maintenance_mode_off, force_maintenance_mode_on


from products.models import Product


class HomeView(ListView):
    model = Product
    queryset = Product.objects.all()
    template_name = 'home.html'
    context_object_name = 'new_products'
    
    def get_queryset(self):
        return super().get_queryset().order_by('-id')[:10]


def contact_us(request):
    if request.method == "POST":
        name = request.POST.get('name')
        email = request.POST.get('email')
        subject = request.POST.get('subject')
        description = request.POST.get('description')


        context = {
            'name': name,
            'email': email,
            'description': description,
        }

        print(context)

        # Send for manager.
        send_mail(
            subject,
            description,
            f'{name}<{email}>',
            [settings.DEFAULT_FROM_EMAIL],
            fail_silently=False,
        )

        # Send for User
        msg_plain = render_to_string('contact_us_email.txt', context)
        msg_html = render_to_string('contact_us_email.html', context)
        to_emails = [email]
        
        email = EmailMultiAlternatives(
            subject=subject,
            body=msg_plain,
            from_email=settings.DEFAULT_FROM_EMAIL, 
            to=to_emails, 
        )

        email.attach_alternative(msg_html, "text/html")
        email.send(fail_silently=True) 

    return render(request,'contact_us.html')


@force_maintenance_mode_off
def my_view_a(request):
    # never return 503 response
    pass

@force_maintenance_mode_on
def my_view_b(request):
    # always return 503 response
    pass
